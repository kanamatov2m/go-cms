package mysql

import (
	"ariumdev.com/go/pkg/models"
	"database/sql"
	"errors"
)

type ArticleModel struct {
	DB *sql.DB
}

func (m *ArticleModel) Insert(title string, content string) (int, error) {
	stmt := "INSERT INTO articles (title, content, created) VALUES(?, ?, UTC_TIMESTAMP())"

	result, errInsert := m.DB.Exec(stmt, title, content)
	if errInsert != nil {
		return 0, errInsert
	}

	id, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}

	return int(id), nil
}

func (m *ArticleModel) Get(id int) (*models.Article, error) {
	stmt := "SELECT id, title, content, created FROM articles WHERE id = ?"

	row := m.DB.QueryRow(stmt, id)

	article := &models.Article{}

	err := row.Scan(&article.ID, &article.Title, &article.Content, &article.Created)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, models.ErrNoRecord
		} else {
			return nil, err
		}
	}

	return article, nil
}

func (m *ArticleModel) Latest() ([]*models.Article, error) {
	return nil, nil
}
